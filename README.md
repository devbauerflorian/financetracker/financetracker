# Welcome to the **Finance Tracker** project

**A quick overview about the main project features:**

![ServiceOverview](/docs/wiki/FinanceTrackerOverview.jpg)

This project implements a complete **Software as a Service (SaaS)** solution for storing and managing financial records in a safe way.

This system is hosted on a private **Raspberry Pi cluster** containing four Raspberry PI 4B running **Kubernetes**.
A compelete **CI/CD Pipeline** is setup and running on this cluster (containing code generation, automated tests, automated documentation and deployment).
The main purpose of this project is **educational**.


**If you want further information about this project checkout the project wiki.**

![Cluster](/docs/wiki/Cluster.jpg)
